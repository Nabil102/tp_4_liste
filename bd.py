import mysql.connector

def obtenir_connexion():
    try :
        connexion = mysql.connector.connect(
                        user="root",
                        password="",
                        host="127.0.0.1",
                        port=3306,
                        database = "peps")
        return connexion
    except mysql.connector.Error as e:
        print(e)