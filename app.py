from flask import Flask
from bd import obtenir_connexion
import json
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return "Liste de boissons peps"

@cross_origin()
@app.route('/items')
def getItems():
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM boissons_peps limit 10")
    resultat = curseur.fetchall()
    liste = []
    for rangee in resultat:
        liste.append(
           {
               "id": rangee[0],
               "nom": rangee[1],
               "format" : rangee[2],
               "prix" : rangee[2]
           } 
        )
    return json.dumps(liste)


@app.route('/items/<rechercher>')
def getOneItem(rechercher):
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT id, nom, format, prix FROM boissons_peps where id=%s or nom like %s", (rechercher,'%' + rechercher + '%'))
    resultat = curseur.fetchall()
    liste = []
    for rangee in resultat:
        liste.append(
           {
                "id": rangee[0],
               "nom": rangee[1],
               "format" : rangee[2],
               "prix" : rangee[2]
           } 
        )
    return json.dumps(liste)

    

 

if __name__ == "__main__":
    app.run(debug=True)